package com.revature.dao;

import com.revature.model.Hand;
import com.revature.model.User;
import com.revature.util.ConnectionUtil;

import java.sql.*;

public class HandDaoImpl implements HandDao{
    /***
     * This method selects all of the hands from the hands table based on the userid of the current user.
     * It prints each hand and its fields to the console.
     * @param user is a User object representing the current user
     */
    @Override
    public void viewHandHistory(User user) {
        String sql = "select * from hands where userid = "+user.getUserid();
        try (Connection connection = ConnectionUtil.getConnection();
             Statement s = connection.createStatement();
             ResultSet rs = s.executeQuery(sql)) {
            ResultSetMetaData rsmd = rs.getMetaData();
            int columnsNumber = rsmd.getColumnCount();
            System.out.println(rsmd.getColumnName(1)+", "+rsmd.getColumnName(2)+", "
                    +rsmd.getColumnName(3)+", "+rsmd.getColumnName(4));
            while(rs.next()){
                for(int i=1; i <= columnsNumber; i++){
                    if(i > 1){
                        System.out.print(", ");
                    }
                    String columnValue = rs.getString(i);
                    System.out.print(columnValue);
                }
                System.out.println("");
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /***
     * This method adds a finished hand to the hands table in our database
     * @param hand is a hand object that is finished.
     * @return true if the hand is added successfully and false otherwise.
     */
    @Override
    public boolean recordHand(Hand hand) {
        String sql = "insert into hands values(default, ?, ?, ?);";
        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql);) {
            ps.setInt(1, hand.getUserId());
            ps.setDouble(2, hand.getBet());
            ps.setString(3, hand.getResult().toString());
            ps.executeUpdate();
            return true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }
    }
}
