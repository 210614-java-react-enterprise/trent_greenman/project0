package com.revature.dao;

import com.revature.model.User;
import com.revature.util.ConnectionUtil;

import java.sql.*;

public class UserDaoImpl implements UserDao{

    /***
     * This method adds a new user to the database in the table called users.
     * @param newUser a User object to be added to our database.
     * @return true if the user was added successfully and false otherwise.
     */
    @Override
    public boolean addUser(User newUser) {
        String sql = "insert into users values(default, ?, ?, ?);";
        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql);) {
            ps.setString(1, newUser.getUsername());
            ps.setString(2, newUser.getPassword());
            ps.setDouble(3, newUser.getBalance());
            ps.executeUpdate();
            return true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }
    }

    /***
     * This method adds a given amount to a user's balance in the users table of our database
     * @param user is the user whose account will be added to
     * @param amount is the amount which will be added to the account.
     * @return a double representing the user's new balance after the deposit.
     */
    @Override
    public double deposit(User user, double amount) {
        String sql = "update users set balance = ? where userid = ?;";
        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql);) {
            ps.setDouble(1, user.getBalance() + amount);
            ps.setInt(2, user.getUserid());
            ps.executeUpdate();
            user.setBalance(user.getBalance() + amount);
            System.out.println("Your new balance is: $"+user.getBalance());
            System.out.println("");
            return user.getBalance();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return -1;
        }
    }

    /***
     * This method subtracts a given amount from a user's balance in the users table of our database. The method
     * does not allow for amounts that are greater than the user's account balance which would result in a negative
     * balance.
     * @param user is the user whose account will be added to
     * @param amount is the amount which will be added to the account.
     * @return a double representing the user's new balance after the withdrawal.
     */
    @Override
    public double withdraw(User user, double amount) {
        String sql = "update users set balance = ? where userid = ?;";
        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql);) {
            if(amount > user.getBalance()){
                System.out.println("Insufficient balance.");
                return -1;
            }
            ps.setDouble(1, user.getBalance() - amount);
            ps.setInt(2, user.getUserid());
            ps.executeUpdate();
            user.setBalance(user.getBalance() - amount);
            System.out.println("Your new balance is: $"+user.getBalance());
            System.out.println("");
            return user.getBalance();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return -1;
        }
    }

    /***
     * This method gets the userid of the current user from the database
     * @param user a User object representing the current user
     * @return an int representing the id of the current user.
     */
    @Override
    public int getUserid(User user) {
        String sql = "select * from users where username = ? and password = ? limit 1;";
        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql);){
            ps.setString(1, user.getUsername());
            ps.setString(2, user.getPassword());
            ResultSet set = ps.executeQuery();
            if (set.next()) {
                user.setUserid(set.getInt("userid"));
                set.close();
                return user.getUserid();
            }else {
                System.out.println("That is not a valid username or password.");
                set.close();
                return -1;
            }
        }catch(SQLException ex){
            ex.printStackTrace();
            return -1;
        }
    }
}
