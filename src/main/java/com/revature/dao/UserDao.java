package com.revature.dao;

import com.revature.model.User;

public interface UserDao {
    public boolean addUser(User user);

    public double deposit(User user, double amount);
    public double withdraw(User user, double amount);
    public int getUserid(User user);
}
