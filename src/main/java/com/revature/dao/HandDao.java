package com.revature.dao;

import com.revature.model.Hand;
import com.revature.model.User;

public interface HandDao {
    public void viewHandHistory(User user);
    public boolean recordHand(Hand hand);
}
