package com.revature.util;

//This enum holds the possible results of a hand. A player can either win, lose, or push.
public enum Result {
    WIN,
    LOSS,
    PUSH
}
