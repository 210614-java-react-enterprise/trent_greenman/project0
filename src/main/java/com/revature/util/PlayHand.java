package com.revature.util;

import com.revature.dao.HandDao;
import com.revature.dao.UserDao;
import com.revature.model.Hand;
import com.revature.model.User;

import java.util.ArrayList;
import java.util.Scanner;

public interface PlayHand {
    public ArrayList<Integer> deal();
    public ArrayList<Integer> hit(ArrayList<Integer> hand);
    public boolean isBlackjack(ArrayList<Integer> hand);
    public int stand(ArrayList<Integer> dealerHand, PlayHandImpl hand, ArrayList<Integer> checkedAceDealer);
    public Result doubleDown(PlayHandImpl hand, ArrayList<Integer> myHand, int handTotal,
                              ArrayList<Integer> checkedAceUser, UserDao userDao, User user, double bet,
                              ArrayList<Integer> dealerHand, ArrayList<Integer> checkedAceDealer);
    public ArrayList<Hand> split(ArrayList<Integer> myHand, PlayHandImpl hand, Scanner scan, double bet,
                                 User user, UserDao userDao, HandDao handDao, ArrayList<Integer> dealerHand);
    public Result evaluate(int dealerHandTotal, UserDao userDao, User user, double bet, int handTotal);
    public int askDoubleDown(Scanner scan, double bet, User user);
    public int askSplit(Scanner scan, double bet, User user);
}
