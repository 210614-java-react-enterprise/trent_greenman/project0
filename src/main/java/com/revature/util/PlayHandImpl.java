package com.revature.util;

import com.revature.dao.HandDao;
import com.revature.dao.UserDao;
import com.revature.dao.UserDaoImpl;
import com.revature.model.Hand;
import com.revature.model.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.Scanner;

public class PlayHandImpl implements PlayHand{
    public HashMap<Integer, String> cardMap = new HashMap<Integer, String>();
    private Random rand = new Random();

    /***
     * This method adds all elements to the card map. It is run once at the beginning of the hand and loads
     * each card string and its integer value into cardMap in order to display information to the user.
     */
    public void loadCardMap(){
        cardMap.put(0, "Ace of Spades");
        cardMap.put(1, "Two of Spades");
        cardMap.put(2, "Three of Spades");
        cardMap.put(3, "Four of Spades");
        cardMap.put(4, "Five of Spades");
        cardMap.put(5, "Six of Spades");
        cardMap.put(6, "Seven of Spades");
        cardMap.put(7, "Eight of Spades");
        cardMap.put(8, "Nine of Spades");
        cardMap.put(9, "Ten of Spades");
        cardMap.put(10, "Jack of Spades");
        cardMap.put(11, "Queen of Spades");
        cardMap.put(12, "King of Spades");
        cardMap.put(13, "Ace of Clubs");
        cardMap.put(14, "Two of Clubs");
        cardMap.put(15, "Three of Clubs");
        cardMap.put(16, "Four of Clubs");
        cardMap.put(17, "Five of Clubs");
        cardMap.put(18, "Six of Clubs");
        cardMap.put(19, "Seven of Clubs");
        cardMap.put(20, "Eight of Clubs");
        cardMap.put(21, "Nine of Clubs");
        cardMap.put(22, "Ten of Clubs");
        cardMap.put(23, "Jack of Clubs");
        cardMap.put(24, "Queen of Clubs");
        cardMap.put(25, "King of Clubs");
        cardMap.put(26, "Ace of Diamonds");
        cardMap.put(27, "Two of Diamonds");
        cardMap.put(28, "Three of Diamonds");
        cardMap.put(29, "Four of Diamonds");
        cardMap.put(30, "Five of Diamonds");
        cardMap.put(31, "Six of Diamonds");
        cardMap.put(32, "Seven of Diamonds");
        cardMap.put(33, "Eight of Diamonds");
        cardMap.put(34, "Nine of Diamonds");
        cardMap.put(35, "Ten of Diamonds");
        cardMap.put(36, "Jack of Diamonds");
        cardMap.put(37, "Queen of Diamonds");
        cardMap.put(38, "King of Diamonds");
        cardMap.put(39, "Ace of Hearts");
        cardMap.put(40, "Two of Hearts");
        cardMap.put(41, "Three of Hearts");
        cardMap.put(42, "Four of Hearts");
        cardMap.put(43, "Five of Hearts");
        cardMap.put(44, "Six of Hearts");
        cardMap.put(45, "Seven of Hearts");
        cardMap.put(46, "Eight of Hearts");
        cardMap.put(47, "Nine of Hearts");
        cardMap.put(48, "Ten of Hearts");
        cardMap.put(49, "Jack of Hearts");
        cardMap.put(50, "Queen of Hearts");
        cardMap.put(51, "King of Hearts");
    }

    /***
     * This method is run at the beginning of a hand and deals two random cards to a user.
     * @return an ArrayList<Integer> with two cards called playerHand
     */
    @Override
    public ArrayList<Integer> deal() {
        int card1 = rand.nextInt(52);
        int card2 = rand.nextInt(52);
        ArrayList<Integer> playerHand = new ArrayList<Integer>();
        playerHand.add(card1);
        playerHand.add(card2);
        return playerHand;
    }

    /***
     * This method is run at the start of a hand to deal a single random card to the dealer
     * @return an ArrayList<Integer> with one card for the dealer called dealerHand
     */
    public ArrayList<Integer> dealDealer(){
        int card = rand.nextInt(52);
        ArrayList<Integer> dealerHand = new ArrayList<Integer>();
        dealerHand.add(card);
        return dealerHand;
    }

    /***
     * This hand determines whether a user's starting hand is a blackjack, meaning the hand
     * contains an ace and 10-K.
     * @param hand is an ArrayList<Integer> of the user's starting hand
     * @return true if the hand is a blackjack and false if it isn't
     */
    @Override
    public boolean isBlackjack(ArrayList<Integer> hand) {
        if(hand.get(0)%13 == 0 && (hand.get(1)%13 >= 9 && hand.get(1)%13 <= 12)){
            System.out.println("You got a blackjack!");
            return true;
        }else if(hand.get(1)%13 == 0 && (hand.get(0)%13 >= 9 && hand.get(0)%13 <= 12)){
            System.out.println("You got a blackjack!");
            return true;
        }else{
            return false;
        }
    }

    /***
     * This methods adds a single random card to the user's hand.
     * @param hand is an ArrayList<Integer> of the user's starting hand
     * @return hand after adding a random card
     */
    @Override
    public ArrayList<Integer> hit(ArrayList<Integer> hand) {
        int card = rand.nextInt(52);
        hand.add(card);
        return hand;
    }

    /***
     * This method adds cards to the dealer's hand until they reach 17 and displays the cards to the user.
     * @param dealerHand is an ArrayList<Integer> that represents the dealer's hand
     * @param hand is a PlayHandImpl object
     * @param checkedAceDealer is an ArrayList<Integer> that keeps track of aces that are given a
     *                         value of 1 instead of 11.
     * @return dealerHandTotal which represents the final hand score of the dealer
     */
    @Override
    public int stand(ArrayList<Integer> dealerHand, PlayHandImpl hand, ArrayList<Integer> checkedAceDealer) {
        System.out.println("");
        //Have the dealer hit until they get 17 or higher
        int dealerHandTotal = 0;
        if(dealerHand.get(0)%13 == 0){
            dealerHandTotal += 11;
        }else if(dealerHand.get(0)%13 >= 9 && dealerHand.get(0)%13 <= 12){
            dealerHandTotal += 10;
        }else{
            dealerHandTotal += dealerHand.get(0)%13 + 1;
        }
        System.out.println("Finishing the dealer hand.");
        System.out.println(hand.cardMap.get(dealerHand.get(0)));
        while(dealerHandTotal < 17){
            hand.hit(dealerHand);
            if(dealerHand.get(dealerHand.size()-1)%13 >= 9 && dealerHand.get(dealerHand.size()-1)%13 <= 12){
                dealerHandTotal += 10;
            }else{
                dealerHandTotal += dealerHand.get(dealerHand.size()-1)%13 + 1;
            }
            System.out.println(hand.cardMap.get(dealerHand.get(dealerHand.size()-1)));

            //If the dealer busts, check for an ace.
            if(dealerHandTotal > 21){
                for(int i=0; i < dealerHand.size(); i++){
                    if(dealerHand.get(i) % 13 == 0 && !checkedAceDealer.contains(i)){
                        dealerHandTotal -= 10;
                        checkedAceDealer.add(i);
                        break;
                    }
                }
            }
        }
        return dealerHandTotal;
    }

    /***
     * This method will double the user's inputted bet and give them only one more card for the hand.
     * It then finishes the dealer's hand by calling the stand method. Next, it calls the stand method
     * and evaluates whether the hand was won or lost and returns that Result object. If the player busts,
     * the new bet amount is withdrawn from their account.
     * @param hand is a PlayHandImpl object
     * @param myHand is an ArrayList<Integer> that represents the user's hand
     * @param handTotal is an int that represents the player's current hand score
     * @param checkedAceUser is an ArrayList<Integer> that keeps track of aces that are given a
     *      *                value of 1 instead of 11 for the player.
     * @param userDao is a UserDao object
     * @param user is the User object that represents the current player
     * @param bet is a double that represents the players bet amount
     * @param dealerHand is an ArrayList<Integer> that contains the dealer's cards.
     * @param checkedAceDealer is an ArrayList<Integer> that keeps track of aces that are given a
     *      *                  value of 1 instead of 11 for the dealer.
     * @return a Result object that tells us whether the player won, lost, or pushed.
     */
    @Override
    public Result doubleDown(PlayHandImpl hand, ArrayList<Integer> myHand, int handTotal,
                              ArrayList<Integer> checkedAceUser, UserDao userDao, User user, double bet,
                              ArrayList<Integer> dealerHand, ArrayList<Integer> checkedAceDealer) {
        //Give the user one card.
        hand.hit(myHand);
        System.out.println("You got the " + hand.cardMap.get(myHand.get(myHand.size() - 1)));
        System.out.println("Your hand is now:");
        for (int i : myHand) {
            System.out.println(hand.cardMap.get(i));
        }

        if(myHand.get(myHand.size()-1)%13 == 0){
            handTotal += 11;
        }else if(myHand.get(myHand.size()-1)%13 >= 9 && myHand.get(myHand.size()-1)%13 <=12){
            handTotal += 10;
        }else{
            handTotal += myHand.get(myHand.size()-1)%13 + 1;
        }

        //check for aces if you bust
        if(handTotal > 21){
            for(int i=0; i < myHand.size(); i++){
                if(myHand.get(i) % 13 == 0 && !checkedAceUser.contains(i)){
                    handTotal -= 10;
                    checkedAceUser.add(i);
                    break;
                }
            }
            if(handTotal > 21){
                System.out.println("BUST. Your hand score is: "+handTotal);
                System.out.println("");
                userDao.withdraw(user, bet);
                return Result.LOSS;
            }
        }
        System.out.println("Your hand score is: "+handTotal);
        System.out.println("");

        //Stand after you get the one card
        if(handTotal <= 21){
            int dealerHandTotal = hand.stand(dealerHand, hand, checkedAceDealer);
            System.out.println("");
            System.out.println("Dealer hand score is: "+dealerHandTotal);
            //Evaluate whether you won, lost, or pushed
            return evaluate(dealerHandTotal, userDao, user, bet, handTotal);
        }
        return null;
    }

    /***
     * This method is called if the user is dealt two of the same cards and chooses to split. From here,
     * this methods plays out two separate hands for the player against a single dealer hand. All other functionality
     * is present for each of the hands such as doubling down, hitting, standing, etc... It then evaluates each hand
     * against the dealer and sets the result of each hand.
     * @param myHand is an ArrayList<Integer> that represents the user's hand
     * @param hand is a PlayHandImpl object
     * @param scan is a Scanner object to take user input
     * @param bet is a double that represents the players bet amount
     * @param user is the User object that represents the current player
     * @param userDao is a UserDao object
     * @param handDao is a HandDao object
     * @param dealerHand is an ArrayList<Integer> that contains the dealer's cards.
     * @return an ArrayList<Hand> that contains two Hand objects with the result set to a Result object
     */
    @Override
    public ArrayList<Hand> split(ArrayList<Integer> myHand, PlayHandImpl hand, Scanner scan, double bet,
                         User user, UserDao userDao, HandDao handDao, ArrayList<Integer> dealerHand) {
        ArrayList<Integer> myHand1 = new ArrayList<Integer>();
        myHand1.add(myHand.get(0));
        hand.hit(myHand1);
        ArrayList<Integer> myHand2 = new ArrayList<Integer>();
        myHand2.add(myHand.get(1));
        hand.hit(myHand2);

        ArrayList<ArrayList<Integer>> handsList = new ArrayList<ArrayList<Integer>>();
        handsList.add(myHand1);
        handsList.add(myHand2);

        Hand userHand1 = new Hand(user.getUserid(), bet);
        Hand userHand2 = new Hand(user.getUserid(), bet);
        ArrayList<Hand> recordHands = new ArrayList<Hand>();
        recordHands.add(userHand1);
        recordHands.add(userHand2);

        ArrayList<Integer> handTotals = new ArrayList<Integer>();

        //Use totalBet to make sure people don't bet more than they have when doubling down after the split
        double totalBet = bet*2;

        for(int i=0; i < handsList.size(); i++){
            ArrayList<Integer> splitHand = handsList.get(i);
            //holds the aces that have a value of 1
            ArrayList<Integer> checkedAceUser = new ArrayList<Integer>();

            //Initialize variables for your hand and the dealer's hand
            int hitOrStand = 0;
            int handTotal = 0;
            for(int j : splitHand){
                if(j%13 == 0){
                    handTotal += 11;
                }else if(j%13 >= 9 && i%13 <=12){
                    handTotal += 10;
                }else{
                    handTotal += j%13 + 1;
                }
            }

            //If you get dealt two aces
            if(handTotal == 22){
                handTotal -= 10;
                checkedAceUser.add(0);
            }

            System.out.println("Your hand is:");
            for(int l : handsList.get(i)){
                System.out.println(hand.cardMap.get(l));
            }
            System.out.println("Your hand score is: "+handTotal);
            System.out.println("");


            int doubleDown = hand.askDoubleDown(scan, bet, user);
            //Makes sure people don't bet more than their balance
            if (doubleDown == 1){
                totalBet += bet;
            }
            if(totalBet > user.getBalance() && doubleDown == 1){
                System.out.println("Insufficient balance to double down.");
                totalBet -= bet;
                doubleDown = 2;
            }

            //Perform action for doubling down--------------------------------------------------------------------------
            if(doubleDown == 1){
                recordHands.get(i).setBet(bet*2);
                System.out.println("You are now betting: $"+bet*2);
                hand.hit(handsList.get(i));
                int lastCard = splitHand.get(splitHand.size()-1);
                System.out.println("You got the "+hand.cardMap.get(lastCard));
                System.out.println("Your hand is now:");
                for (int j : splitHand) {
                    System.out.println(hand.cardMap.get(j));
                }
                if(lastCard%13 == 0){
                    handTotal += 11;
                }else if(lastCard%13 >= 9 && lastCard%13 <=12){
                    handTotal += 10;
                }else{
                    handTotal += lastCard%13 + 1;
                }
                //check for aces if you bust
                if(handTotal > 21){
                    for(int j=0; j < splitHand.size(); j++){
                        if(splitHand.get(j) % 13 == 0 && !checkedAceUser.contains(j)){
                            handTotal -= 10;
                            checkedAceUser.add(j);
                            break;
                        }
                    }
                    if(handTotal > 21){
                        System.out.println("BUST. Your hand score is: "+handTotal);
                        System.out.println("");
                        userDao.withdraw(user, recordHands.get(i).getBet());
                        totalBet -= recordHands.get(i).getBet();
                        //record hand result if the user busts
                        recordHands.get(i).setResult(Result.LOSS);
                        handDao.recordHand(recordHands.get(i));
                        handTotals.add(-1);
                    }else{
                        System.out.println("Your hand total is: "+handTotal);
                        handTotals.add(handTotal);
                        System.out.println("");
                    }
                }else{
                    System.out.println("Your hand score is: "+handTotal);
                    handTotals.add(handTotal);
                    System.out.println("");
                }
                hitOrStand = 1;
            }

            //Perform action for NOT doubling down
            while(hitOrStand != 1 && handTotal <= 21) {
                System.out.println("Which of the following actions would you like to perform? "
                        + "Please select a number corresponding to the action you desire."
                        + "\n1. Stand"
                        + "\n2. Hit");
                try{
                    hitOrStand = scan.nextInt();
                    scan.nextLine();
                }catch(java.util.InputMismatchException e){
                    scan.next();
                }


                //Perform action for stand
                if (hitOrStand == 1) {
                    System.out.println("You chose to stand for this hand with a score of: "+handTotal);
                    handTotals.add(handTotal);

                //Perform action for hit
                } else if (hitOrStand == 2) {
                    hand.hit(splitHand);
                    System.out.println("You got the " + hand.cardMap.get(splitHand.get(splitHand.size() - 1)));
                    System.out.println("Your hand is now:");
                    for (int j : splitHand) {
                        System.out.println(hand.cardMap.get(j));
                    }

                    if(splitHand.get(splitHand.size()-1)%13 == 0){
                        handTotal += 11;
                    }else if(splitHand.get(splitHand.size()-1)%13 >= 9 && splitHand.get(splitHand.size()-1)%13 <=12){
                        handTotal += 10;
                    }else{
                        handTotal += splitHand.get(splitHand.size()-1)%13 + 1;
                    }

                    //check for aces if you bust
                    if(handTotal > 21){
                        for(int j=0; j < myHand.size(); j++){
                            if(splitHand.get(j) % 13 == 0 && !checkedAceUser.contains(j)){
                                handTotal -= 10;
                                checkedAceUser.add(j);
                                break;
                            }
                        }
                        if(handTotal > 21){
                            System.out.println("BUST. Your hand score is: "+handTotal);
                            System.out.println("");
                            userDao.withdraw(user, bet);
                            //record hand result if the user busts
                            recordHands.get(i).setResult(Result.LOSS);
                            handDao.recordHand(recordHands.get(i));
                            handTotals.add(-1);
                            hitOrStand = 1;
                        }
                    }
                    if(hitOrStand != 1){
                        System.out.println("Your hand score is: "+handTotal);
                        System.out.println("");
                    }
                }else{
                    System.out.println("That is not an option. Try again: ");
                }
            }
        }

        //After both hands are settled, finish the dealer hand and evaluate
        ArrayList<Integer> checkedAceDealer = new ArrayList<Integer>();

        int dealerHandTotal = hand.stand(dealerHand, hand, checkedAceDealer);
        System.out.println("Dealer hand score is: "+dealerHandTotal);
        System.out.println("");
        for(int i=0; i < handsList.size(); i++){
            if(recordHands.get(i).getResult() == null){
                Result result = hand.evaluate(dealerHandTotal, userDao, user, recordHands.get(i).getBet(), handTotals.get(i));
                recordHands.get(i).setResult(result);
            }
        }
        return recordHands;
    }

    /***
     * This method evaluates whether a user's finished hand won, lost, or pushed against the dealer's
     * finished hand. If the user won, then the bet amount is deposited into their account. If the user lost,
     * then the bet is withdrawn from their account, and if they pushed, nothing is deposited or withdrawn.
     * @param dealerHandTotal is an int representing the final hand score of the dealer
     * @param userDao a UserDao object
     * @param user is the User object that represents the current player
     * @param bet is a double that represents the players bet amount
     * @param handTotal is an int representing the final hand score of the player
     * @return a Result object based on whether the player won, lost, or pushed.
     */
    @Override
    public Result evaluate(int dealerHandTotal, UserDao userDao, User user, double bet, int handTotal){
        if(dealerHandTotal > 21){
            System.out.println("You won! Dealer busted.");
            userDao.deposit(user, bet);
            return Result.WIN;
        }else if(handTotal > dealerHandTotal){
            System.out.println("You won!");
            userDao.deposit(user, bet);
            return Result.WIN;
        }else if(handTotal == dealerHandTotal){
            System.out.println("Push.");
            System.out.println("");
            return Result.PUSH;
        }else{
            System.out.println("You lost.");
            userDao.withdraw(user, bet);
            return Result.LOSS;
        }
    }

    /***
     * This method displays a message that asks the user if they would like to double down. It will not let
     * the user double down if their balance is insufficient.
     * @param scan is a Scanner object
     * @param bet is a double that represents the players bet amount
     * @param user is the User object that represents the current player
     * @return doubleDown is an int being 1 if they choose to double down and 2 otherwise.
     */
    @Override
    public int askDoubleDown(Scanner scan, double bet, User user) {
        int doubleDown = 0;
        System.out.println("Would you like to double down? Enter 1 for \"Yes\" and 2 for \"No\": ");
        while(doubleDown != 1 && doubleDown != 2){
            try{
                doubleDown = scan.nextInt();
                scan.nextLine();
            }catch(java.util.InputMismatchException e){
                scan.next();
            }

            if(doubleDown == 1){
                //Check to make sure the user has enough money to double down.
                if(bet*2 <= user.getBalance()){
                    System.out.println("You chose to double down.");
                }else{
                    System.out.println("You have an insufficient balance to double down.");
                    System.out.println("");
                    doubleDown = 2;
                }
            }else if(doubleDown == 2){
                System.out.println("You chose to not double down.");
            }else{
                System.out.println("That is not an option. Try again: ");
            }
        }
        return doubleDown;
    }

    /***
     * This method displays a message that asks the user if they would like to split if they are
     * dealt two of the same card value. It will not let the user split if their balance is insufficient.
     * @param scan is a Scanner object
     * @param bet is a double that represents the players bet amount
     * @param user is the User object that represents the current player
     * @return split is an int being 1 if they choose to split and 2 otherwise.
     */
    @Override
    public int askSplit(Scanner scan, double bet, User user) {
        int split = 0;
        System.out.println("Would you like to split? Enter 1 for \"Yes\" and 2 for \"No\": ");
        while(split != 1 && split != 2){
            try{
                split = scan.nextInt();
                scan.nextLine();
            }catch(java.util.InputMismatchException e){
                scan.next();
            }

            if(split == 1){
                //Check to make sure the user has enough money to double down.
                if(bet*2 <= user.getBalance()){
                    System.out.println("You chose to split.");
                }else{
                    System.out.println("You have an insufficient balance to split.");
                    System.out.println("");
                    split = 2;
                }
            }else if(split == 2){
                System.out.println("You chose to not split.");
            }else{
                System.out.println("That is not an option. Try again: ");
            }
        }
        return split;
    }
}
