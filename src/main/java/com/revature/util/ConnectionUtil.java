package com.revature.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionUtil {
    private static Connection connection;

    /***
     * This method creates a connection with a database based on url and PASSWORD and USERNAME which are stored
     * as environment variables.
     * @return a Connection object to interact with a database
     * @throws SQLException
     */
    public static Connection getConnection() throws SQLException {
        /*
        jdbc connection string format: jdbc:postgresql://host:port/database
        optionally if we're accessing a different schema: jdbc:postgresql://host:port/database?currentSchema=schemaName
         */
        String url = "jdbc:postgresql://training-db.coqfdp1depxb.us-east-2.rds.amazonaws.com:5432/postgres";
        //To set these variables, go to BlackjackAppDriver, right click run, click on 'Modify Run Configuration'
        //Type PASSWORD=your password;USERNAME=your username
        final String PASSWORD = System.getenv("PASSWORD");
        final String USERNAME = System.getenv("USERNAME");

        if(connection==null || connection.isClosed()){
            connection = DriverManager.getConnection(url, USERNAME, PASSWORD);
        }

        return connection;
    }


}
