package com.revature.util;

import com.revature.model.User;

import java.sql.*;

public class AuthenticationService {

    /***
     * This method searches through the users table to find a user with a matching username and password. Once
     * a user is found with the matching credentials, a User object is created and its fields are set based on
     * the information from the database.
     * @param username is a String representing the user's username
     * @param password is a String representing the users's password
     * @return a User object if the username and password are found in the database and null otherwise
     */
    public static User loginUserDB(String username, String password) {
        String sql = "select * from users where username = ? and password = ? limit 1;";
        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql);){
            ps.setString(1, username);
            ps.setString(2, password);
            ResultSet set = ps.executeQuery();
            if (set.next()) {
                User user = new User();
                user.setUserid(set.getInt("userid"));
                user.setUsername(set.getString("username"));
                user.setPassword(set.getString("password"));
                user.setBalance(set.getDouble("balance"));
                set.close();
                System.out.println("Login successful.");
                return user;
            }else {
                System.out.println("That is not a valid username or password. Try again.");
                set.close();
                return null;
            }
        }catch(SQLException ex){
            ex.printStackTrace();
            return null;
        }
    }
}
