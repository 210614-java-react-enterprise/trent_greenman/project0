package com.revature.controller;

import com.revature.model.User;
import com.revature.util.ConnectionUtil;

import java.sql.Connection;
import java.sql.SQLException;

public class BlackjackAppDriver {
    public static void main(String[] args){
        BlackjackAppControler controller = new BlackjackAppControler();
        int choice1 = -1;
        while(choice1 < 0 || choice1 > 2){
            choice1 = controller.loginMenu();
            if(choice1 < 0 || choice1 > 2){
                System.out.println("That is not a valid option, please try again.");
            }
        }

        int choice2 = -1;
        User user = controller.startAction(choice1);
        if(choice1 != 0){
            while(choice2 != 0){
                choice2 = controller.menu();
                controller.action(choice2, user);
            }
        }
    }
}
