package com.revature.controller;

import com.revature.dao.HandDao;
import com.revature.dao.HandDaoImpl;
import com.revature.dao.UserDao;
import com.revature.dao.UserDaoImpl;
import com.revature.model.Hand;
import com.revature.model.User;
import com.revature.util.AuthenticationService;
import com.revature.util.PlayHandImpl;
import com.revature.util.Result;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class BlackjackAppControler {
    private static Scanner scan = new Scanner(System.in);

    /***
     * This method displays a menu asking the user if they would like to quit the program, login to an existing account
     * or create a new user.
     * @return an int representing the choice made by the user
     */
    public int loginMenu(){
        System.out.println("Which of the following actions would you like to perform? "
                + "Please select a number corresponding to the action you desire."
                + "\n0. Quit the program"
                + "\n1. Login"
                + "\n2. Create a new user");
        try{
            int choice = scan.nextInt();
            scan.nextLine();
            return choice;
        }catch(java.util.InputMismatchException e){
            scan.next();
            return -1;
        }

    }

    /***
     * This method displays a method once the user has already logged in asking them to select a number from 0-4
     * which is associated with an action.
     * @return an int representing the choice made by the user
     */
    public int menu(){
        System.out.println("Which of the following actions would you like to perform? "
                + "Please select a number corresponding to the action you desire."
                + "\n0. Quit the program"
                + "\n1. Add or remove money from your account"
                + "\n2. View account balance"
                + "\n3. View your hand history"
                + "\n4. Play a hand of Blackjack");
        try{
            int choice = scan.nextInt();
            scan.nextLine();
            return choice;
        }catch(java.util.InputMismatchException e){
            scan.next();
            return -1;
        }

    }

    /***
     * This method performs the action associated with the choice made in the loginMenu method.
     * @param choice is the choice made from the loginMenu method.
     * @return a User object which is either just created or was logged into.
     */
    public User startAction(int choice){
        switch (choice){
            case 0:
                System.out.println("You have quit the program.");
                return null;
            case 1:
                User user = null;
                while(user == null) {
                    System.out.println("Enter username: ");
                    String username = scan.nextLine();
                    System.out.println("Enter password: ");
                    String password = scan.nextLine();
                    user = AuthenticationService.loginUserDB(username, password);
                }
                return user;
            case 2:
                User newUser = new User();
                System.out.println("Enter new username: ");
                String newUsername = scan.nextLine();
                System.out.println("Enter new password: ");
                String newPassword = scan.nextLine();
                System.out.println("Enter starting balance: ");
                Double balance = scan.nextDouble();
                scan.nextLine();
                UserDao userDao = new UserDaoImpl();
                newUser.setUsername(newUsername);
                newUser.setPassword(newPassword);
                newUser.setBalance(balance);
                userDao.addUser(newUser);

                int userid = userDao.getUserid(newUser);
                newUser.setUserid(userid);
                return newUser;
            default:
                System.out.println("That is not a valid option.");
                return null;
        }
    }

    /***
     * This method performs the action associated with the choice made in the menu method. This controls
     * the flow of the majority of the project.
     * @param choice is the choice made from the menu method.
     * @param user is a User object which was created from the startAction menu.
     */
    public void action(int choice, User user){
        UserDao userDao = new UserDaoImpl();
        HandDao handDao = new HandDaoImpl();

        switch (choice){
            case 0:
                System.out.println("You have quit the program.");
                break;
            case 1:
                System.out.println("Which of the following actions would you like to perform? "
                        + "Please select a number corresponding to the action you desire."
                        + "\n1. Withdraw money from account."
                        + "\n2. Deposit money into account.");
                int withOrDep = 0;
                while(withOrDep != 1 && withOrDep != 2){
                    try{
                        withOrDep = scan.nextInt();
                        scan.nextLine();
                    }catch(java.util.InputMismatchException e){
                        withOrDep = -1;
                        scan.next();
                    }

                    if(withOrDep != 1 && withOrDep != 2){
                        System.out.println("That is not a valid option. Try again: ");
                    }
                }
                System.out.println("Enter the amount: ");
                double amount = -1;
                while(amount < 0){
                    try{
                        amount = scan.nextDouble();
                        scan.nextLine();
                        if(amount < 0){
                            System.out.println("Amount must be positive. Try again: ");
                        }
                    }catch(java.util.InputMismatchException e){
                        System.out.println("That is not a valid input. Try again: ");
                        scan.next();
                    }
                }

                if(withOrDep == 1){
                    userDao.withdraw(user, amount);
                }else if(withOrDep == 2){
                    userDao.deposit(user, amount);
                }
                break;
            case 2:
                System.out.println("You have $"+user.getBalance()+" in your account.");
                System.out.println("");
                break;
            case 3:
                handDao.viewHandHistory(user);
                System.out.println("");
                break;
            case 4:
                System.out.println("Play a hand of Blackjack.");
                PlayHandImpl hand = new PlayHandImpl();
                hand.loadCardMap();

                //Take bet amount
                double bet = -1;
                while(bet < 0 || bet > user.getBalance()) {
                    System.out.println("Enter bet amount: ");
                    try{
                        bet = scan.nextDouble();
                        scan.nextLine();
                    }catch(java.util.InputMismatchException e){
                        bet = -1;
                        scan.next();
                    }

                    if(bet < 0){
                        System.out.println("Invalid amount, please try again.");
                    }else if(bet > user.getBalance()){
                        System.out.println("Insufficient balance, try again.");
                    }
                }

                Hand userHand = new Hand(user.getUserid(), bet);

                //holds the aces that have a value of 1
                ArrayList<Integer> checkedAceUser = new ArrayList<Integer>();
                ArrayList<Integer> checkedAceDealer = new ArrayList<Integer>();

                //deal cards to you and the dealer
                ArrayList<Integer> myHand = hand.deal();

                ArrayList<Integer> dealerHand = hand.dealDealer();
                System.out.println("Your hand is:");
                for(int i : myHand){
                    System.out.println(hand.cardMap.get(i));
                }
                System.out.println("");
                System.out.println("The dealer's card is:");
                for(int i : dealerHand){
                    System.out.println(hand.cardMap.get(i));
                }
                System.out.println("");

                //Initialize variables for your hand and the dealer's hand
                int hitOrStand = 0;
                int handTotal = 0;
                for(int i : myHand){
                    if(i%13 == 0){
                        handTotal += 11;
                    }else if(i%13 >= 9 && i%13 <=12){
                        handTotal += 10;
                    }else{
                        handTotal += i%13 + 1;
                    }
                }

                //Check if the cards have the same value for splitting and ask if the user wants to split.
                int split = 0;
                if(myHand.get(0)%13 == myHand.get(1)%13){
                    split = hand.askSplit(scan, bet, user);
                }

                //Perform action for split------------------------------------------------------------------------------
                if(split == 1){
                    ArrayList<Hand> handsList = hand.split(myHand, hand, scan, bet,
                    user, userDao, handDao, dealerHand);
                    for(Hand k : handsList){
                        handDao.recordHand(k);
                    }
                    hitOrStand = 1;
                }

                //If you get dealt two aces and don't split them
                if(handTotal == 22){
                    handTotal -= 10;
                    checkedAceUser.add(0);
                }

                System.out.println("Your hand score is: "+handTotal);

                //Makes sure the user hasn't already split
                if(hitOrStand != 1){
                    //Ask user if they want to double down.-------------------------------------------------------------
                    int doubleDown = hand.askDoubleDown(scan, bet, user);

                    //Perform action for double down.-------------------------------------------------------------------
                    if(doubleDown == 1){
                        bet *= 2;
                        userHand.setBet(bet);
                        System.out.println("You are now betting: $"+bet);
                        Result result = hand.doubleDown(hand, myHand, handTotal, checkedAceUser, userDao, user, bet,
                                dealerHand, checkedAceDealer);
                        //record hand result
                        userHand.setResult(result);
                        handDao.recordHand(userHand);
                        hitOrStand = 1;
                    }
                }

                //Perform action for NOT doubling down------------------------------------------------------------------
                while(hitOrStand != 1 && handTotal <= 21) {

                    //Check if your hand is a blackjack
                    if(myHand.size()==2){
                        if(hand.isBlackjack(myHand)){
                            userDao.deposit(user, bet * 1.5);
                            userHand.setResult(Result.WIN);
                            break;
                        }
                    }

                    System.out.println("Which of the following actions would you like to perform? "
                            + "Please select a number corresponding to the action you desire."
                            + "\n1. Stand"
                            + "\n2. Hit");
                    try{
                        hitOrStand = scan.nextInt();
                        scan.nextLine();
                    }catch(java.util.InputMismatchException e){
                        scan.next();
                    }


                    //Perform action for stand
                    if (hitOrStand == 1) {
                        //int dealerHandTotal = hand.stand(handTotal, dealerHand, hand, checkedAceDealer);
                        int dealerHandTotal = hand.stand(dealerHand, hand, checkedAceDealer);
                        System.out.println("");
                        System.out.println("Dealer hand score is: "+dealerHandTotal);

                        //Evaluate whether you won, lost, or pushed
                        Result result = hand.evaluate(dealerHandTotal, userDao, user, bet, handTotal);
                        //record hand result
                        userHand.setResult(result);
                        handDao.recordHand(userHand);

                    //Perform action for hit
                    } else if (hitOrStand == 2) {
                        hand.hit(myHand);
                        System.out.println("You got the " + hand.cardMap.get(myHand.get(myHand.size() - 1)));
                        System.out.println("Your hand is now:");
                        for (int i : myHand) {
                            System.out.println(hand.cardMap.get(i));
                        }

                        if(myHand.get(myHand.size()-1)%13 == 0){
                            handTotal += 11;
                        }else if(myHand.get(myHand.size()-1)%13 >= 9 && myHand.get(myHand.size()-1)%13 <=12){
                            handTotal += 10;
                        }else{
                            handTotal += myHand.get(myHand.size()-1)%13 + 1;
                        }

                        //check for aces if you bust
                        if(handTotal > 21){
                            for(int i=0; i < myHand.size(); i++){
                                if(myHand.get(i) % 13 == 0 && !checkedAceUser.contains(i)){
                                    handTotal -= 10;
                                    checkedAceUser.add(i);
                                    break;
                                }
                            }
                            if(handTotal > 21){
                                System.out.println("BUST. Your hand score is: "+handTotal);
                                System.out.println("");
                                userDao.withdraw(user, bet);
                                //record hand result
                                userHand.setResult(Result.LOSS);
                                handDao.recordHand(userHand);
                                break;
                            }
                        }
                        System.out.println("Your hand score is: "+handTotal);
                        System.out.println("");
                    }else{
                        System.out.println("That is not an option.");
                    }
                }
                break;
            default:
                System.out.println("That is not an option");
                break;
        }
    }
}
