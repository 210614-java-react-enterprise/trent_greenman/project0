package com.revature.model;

import com.revature.util.Result;

public class Hand {
    public double bet;
    public int userId;
    public int handId;
    public Result result;

    //Constructors
    public Hand(){
        super();
    }

    public Hand(int userId, double bet){
        this.userId = userId;
        this.bet = bet;
    }

    //Getters and Setters
    public double getBet() {
        return bet;
    }

    public void setBet(double bet) {
        this.bet = bet;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getHandId() {
        return handId;
    }

    public void setHandId(int handId) {
        this.handId = handId;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }
}
