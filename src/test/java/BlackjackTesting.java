import com.revature.dao.UserDao;
import com.revature.dao.UserDaoImpl;
import com.revature.model.User;
import com.revature.util.AuthenticationService;
import com.revature.util.PlayHandImpl;
import com.revature.util.Result;
import org.checkerframework.checker.fenum.qual.SwingTextOrientation;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

//*****Make sure to set environment variables PASSWORD and USERNAME for each of these tests.*****
public class BlackjackTesting {
    public static final PlayHandImpl playHand = new PlayHandImpl();
    public static final AuthenticationService authenticationService = new AuthenticationService();

    @Test
    public void testHit(){
        ArrayList<Integer> hand = new ArrayList<>();
        hand.add(3);
        hand.add(50);
        assertEquals(3, playHand.hit(hand).size());
    }

    //isBlackjack() method
    @Test
    public void noBlackjackNumbers(){
        ArrayList<Integer> hand = new ArrayList<Integer>();
        hand.add(6);
        hand.add(7);
        assertFalse(playHand.isBlackjack(hand));
    }

    @Test
    public void noBlackjackFaces(){
        ArrayList<Integer> hand = new ArrayList<Integer>();
        hand.add(10);
        hand.add(11);
        assertFalse(playHand.isBlackjack(hand));
    }

    @Test
    public void noBlackjackAces(){
        ArrayList<Integer> hand = new ArrayList<Integer>();
        hand.add(0);
        hand.add(26);
        assertFalse(playHand.isBlackjack(hand));
    }

    @Test
    public void yesBlackjackFaceFirst(){
        ArrayList<Integer> hand = new ArrayList<Integer>();
        hand.add(11);
        hand.add(0);
        assertTrue(playHand.isBlackjack(hand));
    }

    @Test
    public void yesBlackjackAceFirst(){
        ArrayList<Integer> hand = new ArrayList<Integer>();
        hand.add(0);
        hand.add(9);
        assertTrue(playHand.isBlackjack(hand));
    }

    //loginUserDB method
    @Test
    public void validUser(){
        assertNotNull(authenticationService.loginUserDB("Trent", "1234"));
    }

    @Test
    public void invalidUser(){
        assertNull(authenticationService.loginUserDB("asdfasdf", "dsdfadsf"));
    }

    //getUserid method
    @Test
    public void validGetUserid(){
        UserDao userDao = new UserDaoImpl();
        User user = new User();
        user.setUsername("Trent");
        user.setPassword("1234");
        assertEquals(5, userDao.getUserid(user));
    }

    @Test
    public void invalidGetUserid(){
        UserDao userDao = new UserDaoImpl();
        User user = new User();
        user.setUsername("sadfasdf");
        user.setPassword("adsfdsaf");
        assertEquals(-1, userDao.getUserid(user));
    }
}
